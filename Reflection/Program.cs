﻿namespace Reflection;

static class Program
{
    #region Private Methods

    static void Main()
    {
        var service = new SerializationService(',');
        var f = new F().Get();
        string inputCSV = "I1,I2,I3,I4,I5\r\n1,2,3,4,5";
        string inputJSON = "{\"I1\":1,\"I2\":2,\"I3\":3,\"I4\":4,\"I5\":5}";

        Test(100, 100000, () => service.Serialize(f), "Сериализация");
        Test(100, 100000, () => JsonConvert.SerializeObject(f), "Сериализация (Newtonsoft.Json)");
        Test(100, 100000, () => service.Deserialize<F>(inputCSV), "Десериализация");
        Test(100, 100000, () => JsonConvert.DeserializeObject<F>(inputJSON), "Десериализация (Newtonsoft.Json)");

    }

    private static void PrintTimeResult(double watch1, double watch2, string type)
    {
        Console.WriteLine("\n{0} -> 100 итераций: {1} (ms)", type, watch1);
        Console.WriteLine("{0} -> 100000 итераций: {1} (ms)", type, watch2);
        Console.WriteLine("{0} -> разница времен: {1} (ms)", type, watch2 - watch1);
    }

    private static void Test<T>(int min, int max, Func<T> func, string type)
    {
        Stopwatch watch1 = new();
        watch1.Start();
        for (int i = 0; i < min; i++)
        {
            func();
        }
        watch1.Stop();

        Stopwatch watch2 = new();
        watch2.Start();
        for (int i = 0; i < max; i++)
        {
            func();
        }
        watch2.Stop();
        PrintTimeResult(watch1.Elapsed.TotalMilliseconds, watch2.Elapsed.TotalMilliseconds, type);
    }
    #endregion;
}
