﻿namespace Reflection.Abstractions;

public interface ISerializationService
{
    public string Serialize<T>(T obj);
    public T Deserialize<T>(string obj) where T : class, new();
}
