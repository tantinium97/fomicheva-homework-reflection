namespace Reflection.Services;

public sealed class SerializationService: ISerializationService
{
    #region Private Field

    private readonly char _separator;

    #endregion

    #region Public Constructors 

    public SerializationService(char separator)
    {
        _separator = separator;
    }

    #endregion

    #region Public Methods

    public string Serialize<T>(T obj)
    {
        FieldInfo[] fields = typeof(T).GetFields();

        var names = string.Join(_separator, fields.Select(f => f.Name));
        var values = string.Join(_separator, fields.Select(f => f.GetValue(obj)));

        return string.Join(Environment.NewLine, names, values);
    }

    public T Deserialize<T>(string obj) where T : class, new()
    {
        FieldInfo[] fields = typeof(T).GetFields();
        PropertyInfo[] propertyInfos = typeof(T).GetProperties();

        string[] data = obj.Split('\r');
        string[] names = data[0].Split(_separator);
        string[] values = data[1].Split(_separator);

        T result = new();

        for (int i = 0; i < names.Length; i++)
        {
            var field = fields.FirstOrDefault(x => x.Name == names[i]);
            if (field is not null)
            {
                field.SetValue(result, Convert.ChangeType(values[i], field.FieldType));
            }

            var prop = propertyInfos.FirstOrDefault(x => x.Name == names[i]);
            if (prop is not null)
            {
                prop.SetValue(result, values[i]);
            }
        }
        
        return result;

    }

    #endregion

}