﻿namespace Reflection;

public sealed class F 
{
    #region Public Fields

    public int I1, I2, I3, I4, I5;
    
    #endregion
    
    #region Public Methods

    public F Get() => new() { I1 = 1, I2 = 2, I3 = 3, I4 = 4, I5 = 5 };
    
    #endregion
}
